# Todo App
Todo Desktop app easy to use 

## Description for now
- Web app to do app 


## Features

- **Add Tasks**: Allows adding new tasks with descriptions.
- **Mark Tasks as Completed**: Marks tasks as done.
- **List Tasks**: Displays a list of tasks with details.
- **Data Persistence**: Stores tasks in `tasks.json`.

## Installation

1. Clone the repository.


2. Navigate to the project directory:
   ```shell
   cd todo_app

3. Build the project with Cargo:
   ```shell
   yarn install


## Usage

- Run the application:
   ```shell 
   yarn start


- Follow on-screen prompts to interact with the to-do list after creating a file `tasks.json`

## Examples

- **Adding a Task**: Enter a task description to add it.
- **Marking a Task as Completed**: Use the task number to mark it as done.
- **Quitting**: Choose to quit to save tasks to `tasks.json`.

## Contributing

- Contributions welcome! Open issues or pull requests.

## License

- Licensed under the MIT License (see LICENSE).

## Acknowledgments

- Thanks to the Rust community for support and resources.This project was inspired by practical app development and learning experiences.
Special thanks to the open-source community for support and resources.
