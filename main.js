const { app, BrowserWindow } = require('electron');
const express = require('express');
const path = require('path');
const fs = require('fs');

// Import your Express server
const server = require('./server.js');

let mainWindow = null;

const createWindow = () => {
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true, // Enable nodeIntegration
      contextIsolation: false, // Turn off contextIsolation
    },
  });

  // Load your Express app
  mainWindow.loadURL(`http://localhost:3000`);

  mainWindow.on('closed', () => {
    mainWindow = null;
  });
};

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});
