const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const path = require('path');

const app = express();
const port = 3000;

app.use(bodyParser.json());

// Serve static files from the "static" folder
app.use(express.static(path.join(__dirname, 'static')));

const tasksFilePath = path.join(__dirname, 'tasks.json');

// Initialize tasks with an empty array if tasks.json does not exist
let tasks = [];

// Check if the tasks.json file exists, and create it if it doesn't
if (!fs.existsSync(tasksFilePath)) {
    fs.writeFileSync(tasksFilePath, '[]', 'utf8');
} else {
    // If the file exists, read tasks from tasks.json
    const data = fs.readFileSync(tasksFilePath, 'utf8');
    tasks = JSON.parse(data);
}

// Route to add a new task
app.post('/add-task', (req, res) => {
    const { description } = req.body;

    if (!description) {
        return res.status(400).json({ error: 'Task description is required.' });
    }

    // Add the new task
    const newTask = { id: tasks.length, description, completed: false };
    tasks.push(newTask);

    // Write tasks back to tasks.json
    writeTasksToJson();

    res.status(201).json(newTask);
});

// Route to mark a task as completed
app.post('/mark-task-completed/:taskId', (req, res) => {
    const taskId = parseInt(req.params.taskId);

    if (isNaN(taskId) || taskId < 0 || taskId >= tasks.length) {
        return res.status(400).json({ error: 'Invalid task ID.' });
    }

    tasks[taskId].completed = true;

    // Write tasks back to tasks.json
    writeTasksToJson();

    res.sendStatus(200);
});

// Route to delete a task
app.delete('/delete-task/:taskId', (req, res) => {
    const taskId = parseInt(req.params.taskId);

    if (isNaN(taskId) || taskId < 0 || taskId >= tasks.length) {
        return res.status(400).json({ error: 'Invalid task ID.' });
    }

    tasks.splice(taskId, 1);

    // Update task IDs to maintain consistency
    tasks.forEach((task, index) => {
        task.id = index;
    });

    // Write tasks back to tasks.json
    writeTasksToJson();

    res.sendStatus(204);
});

// Route to get all tasks
app.get('/get-tasks', (req, res) => {
    res.json(tasks);
});

// Helper function to write tasks to tasks.json
function writeTasksToJson() {
    fs.writeFileSync(tasksFilePath, JSON.stringify(tasks, null, 4), 'utf8');
}

// Start the server
const server = app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
